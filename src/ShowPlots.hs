module ShowPlots where

import           CommonTypes

import           Control.Monad                          (when)

import           Control.Lens
import           Data.Colour
import           Data.Colour.Names

import           Graphics.Rendering.Chart.Easy
import           Graphics.Rendering.Chart.Gtk

import           Data.Default
import           Data.Maybe                             (fromJust, isJust)
import           Graphics.Rendering.Chart
import           Graphics.Rendering.Chart.Backend.Cairo

type NameAndFilteredTrack = (String, [Position], [Speed])

view :: Maybe [Position] -> [NameAndFilteredTrack] -> IO ()
view maybeTrack infos =
    toWindow 300 300 $ do
        layoutlr_title .= "Track & filtered-output"
        layoutlr_x_axis . laxis_title .= "time"
        layoutlr_left_axis . laxis_title .= "Position"
        layoutlr_right_axis . laxis_title .= "speed"

        layoutlr_title_style . font_size .= 18
        {-layout_title_style . font_weight .= FontWeightNormal-}
        layoutlr_x_axis . laxis_title_style . font_size .= 18
        layoutlr_x_axis . laxis_style . axis_label_style . font_size .= 14
        layoutlr_left_axis . laxis_title_style . font_size .= 16
        layoutlr_right_axis . laxis_title_style . font_size .= 16
        layoutlr_left_axis . laxis_style . axis_label_style . font_size .= 14
        layoutlr_right_axis . laxis_style . axis_label_style . font_size .= 14
        layoutlr_legend .= Just def

        layoutlr_right_axis . laxis_generate .= scaledAxis def (-1,4)
        setColors [opaque red, opaque blue, opaque darkgreen, opaque darkorange]
        when (isJust maybeTrack) $
            plotLeft (points "track" (zip [1 ..] (unPos <$> fromJust maybeTrack)))
        mapM_
            (\(name, filteredTrack, speeds) -> do
                 plotLeft
                     (line
                          name
                          [zip ([1 ..] :: [Double]) (unPos <$> filteredTrack)])
                 plotRight
                     (line
                        ("speed-" ++ name)
                        [zip ([1 ..] :: [Double]) (unSpeed <$> speeds)])
            ) infos
