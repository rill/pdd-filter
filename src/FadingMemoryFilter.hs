{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module FadingMemoryFilter where

import           CommonTypes
import           FilterState

newtype FadingMemoryState a = FadingMemoryState a

instance FilterState (FadingMemoryState Position) Position where
    singleStep (Position x) = do
        FadingMemoryState (Position prev) <- get
        let alpha = 0.85
            mean = Position $ (alpha * prev) + ((1 - alpha) * x)
        put $ FadingMemoryState mean
        return mean
    initialState = FadingMemoryState $ Position 0
    likelihood (FadingMemoryState mean) y = Likelihood $ exp (-0.5 * d ^ 2)
      where
        d = unPos y / unPos mean
    getSpeed = undefined
