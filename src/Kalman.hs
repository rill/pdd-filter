{-# LANGUAGE DataKinds #-}

module Kalman where

import           Data.Maybe                   (fromJust)
import           GHC.TypeLits
import           Numeric.LinearAlgebra.Static

kalmanStep ::
       (KnownNat n, KnownNat m)
    => Sq n
    -> Sym n
    -> L m n
    -> Sym m
    -> (R n, Sym n)
    -> R m
    -> (R n, Sym n, R m, Sym m)
kalmanStep ϕMatrix qMatrix hMatrix rMatrix (x, p) y =
    (x', p', res, sym sMatrix)
  where
    xMinus = ϕMatrix #> x
    pMinus = ϕMatrix <> unSym p <> tr ϕMatrix + unSym qMatrix
    sMatrix = hMatrix <> pMinus <> tr hMatrix + unSym rMatrix
    kMatrix = pMinus <> tr hMatrix <> unsafeInv sMatrix
    res = y - hMatrix #> xMinus
    x' = xMinus + kMatrix #> res
    p' = sym (pMinus - kMatrix <> sMatrix <> tr kMatrix)

unsafeInv :: KnownNat n => Sq n -> Sq n
unsafeInv m = fromJust $ linSolve m eye
