module CommonTypes where

newtype Time = Time Int deriving (Show, Eq)
newtype Position = Position { unPos :: Double } deriving Show
newtype Speed = Speed { unSpeed :: Double } deriving Show
