{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module ConstantVelocityFilter where

import           CommonTypes
import           FilterState
import           Kalman
import           Numeric.LinearAlgebra.Static

data ConstantVelocityState = ConstantVelocityState
    { getX :: R 2
    , getP :: Sym 2
    , getR :: R 1
    , getS :: Sym 1
    } deriving (Show)

instance FilterState ConstantVelocityState Position where
    singleStep (Position input) = do
        ConstantVelocityState x p _ _ <- get
        let y = vector [input] :: R 1
            (x', p', res, sMatrix) =
                kalmanStep phiMatrix qMatrix hMatrix rMatrix (x, p) y
            output = (Position . fst . headTail) x'
        put $ ConstantVelocityState x' p' res sMatrix
        return output
      where
        phiMatrix = matrix [ 1, deltaT
                           , 0, 1] :: Sq 2
          where
            deltaT = 1
        hMatrix = matrix [1, 0] :: L 1 2
        rMatrix = sym (matrix [2] :: L 1 1)
        qMatrix =
            sym $
            matrix
                [ phiS * (deltaT ^ 3) / 3, phiS * (deltaT ^ 2) / 2
                , phiS * (deltaT ^ 2) / 2, phiS * deltaT ]
          where
            phiS :: Double
            phiS = 1e-3 {- increase this to allow tolerance for uncertainty
                           or for faster response times -}
            deltaT = 1
    initialState =
        ConstantVelocityState
        { getX = vector [0{- position -}, 0{- speed -}]
        , getP = sym $ matrix [ 1000, 0
                              , 0, 1000 ]
        , getR = vector [1]
        , getS = sym $ matrix [0]
        }
    likelihood state _ = Likelihood $ exp ( -0.5 * dSq ) / sqrt norm
        where y = getR state
              s = getS state
              sInv = unsafeInv $ unSym s
              dSqMatrix = col y <> sInv <> row y
              dSq = fst . headTail . unrow $ dSqMatrix
              norm = (det . unSym) s
    getSpeed = Speed . fst . headTail . snd . headTail . getX
