{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module AveragingFilter where

import           CommonTypes
import           FilterState

newtype Count = Count Int
newtype AveragingState a = AveragingState (a, Count)

instance FilterState (AveragingState Position) Position where
    singleStep (Position y) = do
        AveragingState (Position x, Count n) <- get
        let newN = n + 1
            newX = x + (y - x) / fromIntegral newN
        put $ AveragingState (Position newX, Count newN)
        return $ Position newX
    initialState = AveragingState (Position 0, Count 0)
    likelihood = undefined
    getSpeed = undefined
