{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module SlidingWindowFilter where

import           CommonTypes
import           FilterState

newtype Count = Count Int
newtype SlidingWindowState a = SlidingWindowState (a, [a])

instance FilterState (SlidingWindowState Position) Position where
    singleStep y = do
        SlidingWindowState (Position sum, ps) <- get
        let windowSize = 10
            ps' = ps ++ [y]
            len' = length ps'
            slide = len' == (windowSize + 1)
            newLength = min len' windowSize
            newSum = if slide
                         then sum - unPos (head ps') + unPos y
                         else sum + unPos y
            newPs = if slide then tail ps' else ps'
        put $ SlidingWindowState (Position newSum, newPs)
        return $ Position (newSum / fromIntegral newLength)
    initialState = SlidingWindowState (Position 0, [])
    likelihood = undefined
    getSpeed = undefined
