{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module StationaryFilter where

import           CommonTypes
import           FilterState
import           Kalman

import           Numeric.LinearAlgebra.Static

data StationaryState = StationaryState
    { getX :: R 1
    , getP :: Sym 1
    , getR :: R 1
    , getS :: Sym 1
    } deriving (Show)

instance FilterState StationaryState Position where
    singleStep (Position input) = do
        StationaryState x p _ _ <- get
        let y = vector [input] :: R 1
            (x', p', res, sMatrix) =
                kalmanStep phiMatrix qMatrix hMatrix rMatrix (x, p) y
            output = (Position . fst . headTail) x'
        put $ StationaryState x' p' res sMatrix
        return output
      where
        phiMatrix = eye :: Sq 1
        hMatrix = matrix [1] :: L 1 1
        rMatrix = sym (matrix [1] :: L 1 1)
        qMatrix = sym $ matrix [phiS * deltaT]
          where
            phiS = 1e-4 {- increase this to allow tolerance for uncertainty
                           or for faster response times -}
            deltaT = 1
    initialState =
        StationaryState
        { getX = vector [0{- position -}]
        , getP = sym $ matrix [1000]
        , getR = vector [0]
        , getS = sym $ matrix [0]
        }
    likelihood state _ = Likelihood $ exp ( -0.5 * dSq ) / sqrt norm
        where y = getR state
              s = getS state
              sInv = unsafeInv $ unSym s
              dSqMatrix = col y <> sInv <> row y
              dSq = fst . headTail . unrow $ dSqMatrix
              norm = (det . unSym) s
    getSpeed _ = Speed 0
