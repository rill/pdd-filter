module TrackGenerator
    ( linearTrackGenerator
    , linearMotionWithSpeed
    , stationary
    , addNoise
    , Sigma(..)
    , at
    , step
    , GeneratorState(GeneratorState)
    ) where

import           CommonTypes

import           Control.Monad.Trans.State

import           Data.Random.Normal
import           System.Random             (mkStdGen)

data GeneratorState = GeneratorState
    { getTime     :: Time
    , getPosition :: Position
    } deriving (Show)
newtype Sigma = Sigma Double

linearTrackGenerator :: State GeneratorState Position
linearTrackGenerator = do
    s <- get
    let Time t = getTime s
        speed =
            Speed $
            if t < 50
                then 0
                else if t < 60
                         then 0.1 * (fromIntegral t - 50)
                         else 1
    linearMotionWithSpeed speed >>= addNoise (Sigma 1)

at :: Time
   -> (Position -> Position)
   -> Position
   -> State GeneratorState Position
at t xform pos = do
    GeneratorState sT sPos <- get
    if t == sT
        then do
            put $ GeneratorState sT (xform sPos)
            return $ xform pos
        else
            return pos

step :: Position -> Position -> Position
step (Position s) (Position p) = Position (s+p)

stationary :: State GeneratorState Position
stationary = do
    GeneratorState (Time t) pos <- get
    let deltaTime = 1
        updatedTime = Time $ t + deltaTime
    put $ GeneratorState updatedTime pos
    return pos

linearMotionWithSpeed :: Speed -> State GeneratorState Position
linearMotionWithSpeed (Speed speed) = do
    GeneratorState (Time t) (Position x) <- get
    let deltaTime = 1
        updatedTime = Time $ t + deltaTime
        updatedPos = Position $ x + speed * fromIntegral deltaTime
    put $ GeneratorState updatedTime updatedPos
    return updatedPos

addNoise :: Sigma -> Position -> State GeneratorState Position
addNoise (Sigma dev) x = do
    GeneratorState (Time t) (Position x) <- get
    let gen = mkStdGen t
        noise = fst $ normal' (0, dev) gen
        x' = Position $ x + noise
    return x'
