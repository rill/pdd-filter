{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}

module FilterState
    ( filteredUsing
    , filterStates
    , evalFilter
    , FilterState(..)
    , Likelihood(..)
    , runFilter
    , module Control.Monad.Trans.State
    ) where

import           CommonTypes               (Speed)
import           Control.Monad.Trans.State

type Filter s = State s
newtype Likelihood = Likelihood Double

runFilter :: Filter s a -> s -> (a, s)
runFilter = runState

evalFilter :: Filter s a -> s -> a
evalFilter = evalState

class FilterState s a | s -> a where
    initialState :: s
    singleStep :: a -> Filter s a
    likelihood :: s -> a -> Likelihood
    getSpeed :: s -> Speed

filteredUsing :: (Traversable t, FilterState s a) => t a -> s -> t a
filteredUsing ys = evalFilter (multiStep ys)
  where
    multiStep :: (Traversable t, FilterState s a) => t a -> Filter s (t a)
    multiStep = traverse singleStep

filterStates :: (Traversable t, FilterState s a) => t a -> s -> t s
filterStates ys = evalFilter (multiStates ys)
  where
    nextState x = singleStep x >> get
    multiStates :: (Traversable t, FilterState s a) => t a -> Filter s (t s)
    multiStates = traverse nextState
