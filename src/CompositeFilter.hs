{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module CompositeFilter where

import           CommonTypes
import           ConstantVelocityFilter as CV
import           FilterState
import           StationaryFilter       as S

{-import           Debug.Trace-}

newtype ComponentWeight = Weight Double deriving Show

data CompositeFilterState = Composite
    { getCV       :: ConstantVelocityState
    , getCVWeight :: ComponentWeight
    , getS        :: StationaryState
    , getSWeight  :: ComponentWeight
    }

instance FilterState CompositeFilterState Position where
    singleStep input = do
        Composite cvState cvWeight sState sWeight <- get
        let
            -- progress states
            (cvPos, cvState') = runFilter (singleStep input) cvState
            (sPos, sState') = runFilter (singleStep input) sState

            -- fetch likelihoods
            cvLikelihood = likelihood cvState' cvPos
            sLikelihood = likelihood sState' sPos

            -- progress weights
            (cvWeight', sWeight') =
                mergeWeights cvWeight cvLikelihood sWeight sLikelihood

        put $ Composite cvState' cvWeight' sState' sWeight'
        return $ mergedPosition cvPos cvWeight' sPos sWeight'
      where
        mergeWeights (Weight w1) (Likelihood l1) (Weight w2) (Likelihood l2) =
            (Weight w1'', Weight w2'')
          where
            w1l1 = w1 * l1
            w2l2 = w2 * l2
            denom = w1l1 + w2l2
            w1' = w1l1 / denom
            w2' = w2l2 / denom
            (w1'', w2'')
                | w1' < 1.0e-3 = (1.0e-3, 1 - 1.0e-3)
                | w2' < 1.0e-3 = (1 - 1.0e-3, 1.0e-3)
                | otherwise = (w1', w2')
        mergedPosition (Position p1) (Weight w1) (Position p2) (Weight w2) =
            Position $ w1 * p1 + w2 * p2
    initialState = Composite initialState (Weight 0.5) initialState (Weight 0.5)
    likelihood = undefined
    getSpeed (Composite cvState cvWeight sState sWeight) =
        {-trace (show sWeight ++ ", " ++ show cvWeight)-}
            blendSpeeds (getSpeed cvState) cvWeight (getSpeed sState) sWeight
      where
        blendSpeeds (Speed s1) (Weight w1) (Speed s2) (Weight w2) =
            Speed $ w1 * s1 + w2 * s2
