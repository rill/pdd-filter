module Main where

import           CommonTypes
import           FilterState
import           ShowPlots
import           TrackGenerator

import           CompositeFilter

import           Control.Monad   (replicateM)

main = do
    let
        compositePositionEstimates =
            track `filteredUsing` (initialState :: CompositeFilterState)
        compositeStateEstimates =
            track `filterStates` (initialState :: CompositeFilterState)
        compositeSpeeds = getSpeed <$> compositeStateEstimates

    view
        {-(Just track)-}
        Nothing
        [
            ("composite", compositePositionEstimates, compositeSpeeds)
        ]

track :: [Position]
track = evalState trackGenerator trackStart
  where
    trackGenerator = replicateM 250 linearTrackGenerator
    trackStart = GeneratorState (Time 0) (Position 0)
